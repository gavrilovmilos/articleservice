'use strict'

const env = process.env.NODE_ENV || "development",
  config = require('../config.json')[env],
  express = require('express'),
  bodyParser = require('body-parser'),
  db = require('./db'),
  log4js = require('log4js'),
  logger = require('./logger')('app'),
  app = express();

// HTTP Routes
var articles = require('./routes/articles');

app.use(log4js.connectLogger(logger))
app.use(bodyParser.json({limit: '1mb'}))

app.use('/v1/articles', articles);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  logger.error('ERROR ...' + err)

  // render the error page
  res.status(err.status || 500);
  res.send()
});

db.sequelize.sync().then(function () {
  var server = app.listen(config.port, function () {
    var host = server.address().address,
      port = server.address().port

    app.emit('listens', null)

    console.log('Article service is listening at http://%s:%s', host, port)
  }).on('error', onError)
})

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error('Port ' + port + ' requires elevated privileges. Shutting down process..')
      process.exit(1)
      break
    case 'EADDRINUSE':
      logger.error('Port ' + port + ' is already in use. Shutting down process..')
      process.exit(1)
      break
    default:
      throw error
  }
}

module.exports = app;