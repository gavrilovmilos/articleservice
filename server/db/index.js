"use strict"

const Sequelize = require("sequelize"),
  env = process.env.NODE_ENV || "development",
  config = require('../../config.json')[env]["db"],
  sequelize = new Sequelize(config.database, config.username, config.password, config),
  db = {}

var models = require('sequelize-import')(__dirname, sequelize, {
  exclude: ["index.js"]
})

db.sequelize = sequelize
db.Sequelize = Sequelize
db.model = models

module.exports = db