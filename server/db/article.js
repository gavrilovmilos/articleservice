'use strict'

module.exports = function (sequelize, DataTypes) {
  return sequelize.define("article", {
    ID: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    articleName: {
      type: DataTypes.UUID,
      field: 'article_name',
      allowNull: false
    },
    title: DataTypes.STRING,
    text: DataTypes.TEXT,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      defaultValue: sequelize.literal('NOW()')
    }
  })
}