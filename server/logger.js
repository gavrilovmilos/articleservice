"use strict"
/*
 * ***************  LOGGER CONFIGURATION MODULE *******************
 * 
 * This module is used for setting up logger configuration, it returns logger object
 * constructor function based on that configuration.
 * Constructor function has one parameter, logger NAME.
 */
var log4js = require('log4js');

var config = function () {
  //See https://github.com/nomiddlename/log4js-node/wiki/Appenders for log4js configuration capabilities.
  switch (process.env.NODE_ENV) {
    case null:
    case undefined:
    case "development":
      return {
        appenders: {
          console: {
            type: 'console'
          }
        },
        categories: { default: { appenders: ['console'], level: 'debug' } }
      }
  }
}

log4js.configure(config());

module.exports = log4js.getLogger