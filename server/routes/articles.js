'use strict'

const express = require('express'),
  router = express.Router(),
  articleService = require('../service/article_service'),
  logger = require('../logger')('article_routes')

// Create article
router.post('/', function (req, res, next) {
  logger.info('Create article: ' + JSON.stringify(req.body))
  articleService.saveArticle(req.body, res)
});

// GET all active articles
router.get('/', function (req, res, next) {
  articleService.getAllActiveArticles(res)
});

// Get article
router.get('/:articleName', function (req, res, next) {
  if (req.query.active) {
    // Return only active article version
    return articleService.getActiveVersion(req.params.articleName, res)
  }

  // Return all versions of the given article
  articleService.getAllVersions(req.params.articleName, res)
});

// GET specific version of article
router.get('/:articleName/:version', function (req, res, next) {
  articleService.getArticleVersion(req.params.articleName, req.params.version, res)
});

// Update article by creating new version
router.post('/:articleName', function (req, res, next) {
  logger.info('Update article[' + req.params.articleName + ']: ' + JSON.stringify(req.body))
  articleService.updateArticle(req.params.articleName, req.body, res)
});

// Bulk update enables saving multiple drafts of the same article
router.post('/:articleName/bulk', function (req, res, next) {
  logger.info('Bulk update article[' + req.params.articleName + ']: ' + JSON.stringify(req.body))
  articleService.bulkUpdate(req.params.articleName, req.body, res)
});

// Publish article version
router.put('/:articleName/:version/publish', function (req, res, next) {
  logger.info('Publish article[' + req.params.articleName + '] version[' + req.params.version + '].')
  articleService.publish(req.params.articleName, req.params.version, res)
});

// Unpublish article version
router.put('/:articleName/:version/unpublish', function (req, res, next) {
  logger.info('Unpublish article[' + req.params.articleName + '] version[' + req.params.version + '].')
  articleService.unpublish(req.params.articleName, req.params.version, res)
});

module.exports = router;