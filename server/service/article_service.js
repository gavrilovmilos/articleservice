'use strict'

const uuidv4 = require('uuid/v4'),
  db = require('../db/index'),
  logger = require('../logger')('article_service')

module.exports = {

  saveArticle: (article, res) => {
    var newArticle = {
      articleName: uuidv4(),
      title: article.title,
      text: article.text
    }

    db.model.article.create(newArticle)
      .then((savedArticle) => {
        res.send(savedArticle);
      })
  },

  getAllActiveArticles: function (res) {
    db.model.article.findAll({where: {active: true}})
      .then((articles) => {
        res.send(articles)
      })
  },

  getActiveVersion: function (articleName, res) {
    db.model.article.findOne({where: {articleName: articleName, active: true}})
      .then((article) => {
        if (!article) {
          return res.status(404).send()
        }
        res.send(article)
      })
  },

  getAllVersions: function (articleName, res) {
    db.model.article.findAll({where: {articleName: articleName}, order: [['version', 'ASC']]})
      .then((article) => {
        if (!article) {
          return res.status(404).send()
        }
        res.send(article)
      })
  },

  getArticleVersion: function (articleName, version, res) {
    db.model.article.findOne({where: {articleName: articleName, version: version}})
      .then((article) => {
        if (!article) {
          return res.status(404).send()
        }
        res.send(article)
      })
  },

  updateArticle: function (articleName, newArticle, res) {
    return db.sequelize.transaction((t) => {
      return db.model.article.count({where: {articleName: articleName}, transaction: t})
        .then(function (versionsCount) {
          logger.info('Found [' + versionsCount + '] versions for article with name [' + articleName + '].')
          var lastVersion = versionsCount
          if (newArticle.version && newArticle.version !== lastVersion + 1) {
            res.status(409).send()
            return
          }
          var convertedArticle = {
            articleName: articleName,
            version: lastVersion + 1,
            active: false,
            text: newArticle.text,
            title: newArticle.title
          }
          return db.model.article.create(convertedArticle, {transaction: t}).then(function (article) {
            return article
          })
        })

    }).then((result) => {
      res.send(result)
    }).catch((err) => {
      logger.error('An error occurred during update operation: ' + err)
      res.status(500).send()
    });
  },

  bulkUpdate: function (articleName, drafts, res) {
    db.sequelize.transaction((t) => {
      return db.model.article.count({where: {articleName: articleName}, transaction: t})
        .then((versionsCount) => {
          logger.info('Found [' + versionsCount + '] versions for article with name [' + articleName + '].')
          var lastVersion = versionsCount
          var convertedDrafts = []

          for (let i = 0; i < drafts.length; i++) {
            let draft = {}
            draft.version = lastVersion + i + 1
            draft.articleName = articleName
            draft.active = false
            draft.title = drafts[i].title
            draft.text = drafts[i].text

            convertedDrafts[i] = draft
          }

          return db.model.article.bulkCreate(convertedDrafts, {transaction: t}).then(function (articleVersions) {
            return articleVersions
          })

        })
    }).then((result) => {
      res.send(result)
    }).catch((err) => {
      logger.error('An error occurred during bulk update operation: ' + err)
      res.status(500).send()
    });
  },

  publish: function (articleName, version, res) {
    db.sequelize.transaction((t) => {
      return db.model.article.update({active: false}, {where: {articleName: articleName, active: true}, transaction: t})
        .then((affected) => {
          return db.model.article.update({active: true}, {where: {articleName: articleName, version: version}, transaction: t})
        })
    })
      .then((affected) => {
        return db.model.article.findOne({where: {articleName: articleName, version: version}})
          .then((article) => {
            res.send(article)
          })
      })
      .catch((err) => {
        logger.error('An error occurred during publish operation: ' + err)
        res.status(500).send()
      });
  },

  unpublish: function (articleName, version, res) {
    db.model.article.update({active: false}, {where: {articleName: articleName, version: version}})
      .then(() => {
        db.model.article.findOne({where: {articleName: articleName, version: version}})
          .then((article) => {
            res.send(article)
          })
      })
  }
}
