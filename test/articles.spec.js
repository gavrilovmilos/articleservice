'use strict'

const request = require('supertest'),
  expect = require('chai').expect,
  app = require('../index')

describe('Article service tests', () => {

  var article = {
    "title": "Crypto article",
    "text": "This is article about cryptography.\n*$#$(DC(#@*$@#(@#(@)1"
  }

  before(function (done) {
    app.on('listens', function () {
      done()
    });
  })

  it('should create article', (done) => {
    request(app).post('/v1/articles')
      .set('Accept', 'application/json')
      .send(article)
      .expect(200)
      .then((res) => {
        console.log('Created article: ' + JSON.stringify(res.body))
        article.articleName = JSON.parse(res.text).articleName
        done()
      })
      .catch((err) => {
        done(new Error(err));
      });
  })

  it('should get an article', (done) => {
    request(app).get('/v1/articles/' + article.articleName)
      .set('Accept', 'application/json')
      .expect(200)
      .then((res) => {
        console.log('Response:   ' + JSON.stringify(res))
        console.log('Response text:   ' + JSON.stringify(res.text))
        console.log('Response body:   ' + JSON.stringify(res.body))
        console.log('Response aname:   ' + res.body.articleName)
        expect(res.body.length).to.equal(1)
        expect(res.body[0].articleName).to.equal(article.articleName)
        done()

      })
      .catch((err) => {
        done(new Error(err));
      });
  })

  it('should update article(create new article version)', (done) => {
    article.title = 'Updated title'
    request(app).post('/v1/articles/' + article.articleName)
      .set('Accept', 'application/json')
      .send(article)
      .expect(200)
      .then((res) => {
        console.log('Updated article: ' + JSON.stringify(res.body))
        expect(res.body.articleName).to.equal(article.articleName)
        expect(res.body.title).to.equal(article.title)
        expect(res.body.version).to.equal(2)
        expect(res.body.active).to.equal(false)

        // Get specific article version
        return request(app).get('/v1/articles/' + article.articleName + '/' + 2)
          .set('Accept', 'application/json')
          .expect(200)
          .then((getRes) => {
            console.log('Response:   ' + JSON.stringify(getRes.body))
            expect(getRes.body.articleName).to.equal(article.articleName)
            expect(getRes.body.title).to.equal(article.title)
            expect(getRes.body.version).to.equal(2)
            done()
          })
      })
      .catch((err) => {
        done(new Error(err));
      });
  })

  // Publish article
  it('should publish specific article version', (done) => {
    request(app).put('/v1/articles/' + article.articleName + '/' + 2 + '/publish')
      .set('Accept', 'application/json')
      .send(article)
      .expect(200)
      .then((res) => {
        console.log('Published article: ' + JSON.stringify(res.body))
        expect(res.body.articleName).to.equal(article.articleName)
        expect(res.body.title).to.equal(article.title)
        expect(res.body.version).to.equal(2)
        expect(res.body.active).to.equal(true)

        // Get active version of an article
        return request(app).get('/v1/articles/' + article.articleName + '?active=true')
          .set('Accept', 'application/json')
          .expect(200)
          .then((getRes) => {
            console.log('Response:   ' + JSON.stringify(getRes.body))
            expect(getRes.body.active).to.equal(true)
            expect(getRes.body.title).to.equal(article.title)
            expect(getRes.body.version).to.equal(2)
            done()
          })
      })
      .catch((err) => {
        done(new Error(err));
      });
  })

  it('should update article by creating multiple versions in bulk', (done) => {
    article.title = 'Updated title'

    var bulkUpdate = [
      {
        "title": "Updated title 1"
      },
      {
        "title": "Updated title 2",
        "text": "Draft version of text"
      }
    ]

    request(app).post('/v1/articles/' + article.articleName + '/bulk')
      .set('Accept', 'application/json')
      .send(bulkUpdate)
      .expect(200)
      .then((res) => {
        console.log('Bulk update result: ' + JSON.stringify(res.body))
        expect(res.body.length).to.equal(2)
        expect(res.body[1].title).to.equal(bulkUpdate[1].title)

        // Get article version history
        return request(app).get('/v1/articles/' + article.articleName)
          .set('Accept', 'application/json')
          .expect(200)
          .then((getRes) => {
            console.log('All article versions: ' + JSON.stringify(getRes.body))
            expect(getRes.body.length).to.equal(4)
            expect(getRes.body[2].title).to.equal(bulkUpdate[0].title)
            expect(getRes.body[2].active).to.equal(false)
            expect(getRes.body[3].title).to.equal(bulkUpdate[1].title)
            expect(getRes.body[3].text).to.equal(bulkUpdate[1].text)
            expect(getRes.body[3].active).to.equal(false)
            done()
          })
      })
      .catch((err) => {
        done(new Error(err));
      });
  })
})